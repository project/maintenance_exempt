<?php

namespace Drupal\maintenance_exempt;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\MaintenanceMode;
use Drupal\Core\Site\MaintenanceModeInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the default implementation of the maintenance mode service.
 */
class MaintenanceModeExempt extends MaintenanceMode implements MaintenanceModeInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The path matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  private $pathMatcher;

  /**
   * The current path stack service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private $currentPathStack;

  /**
   * Constructs a new maintenance mode service.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   The current path stack service.
   */
  public function __construct(StateInterface $state, ConfigFactoryInterface $config_factory, RequestStack $request_stack, PathMatcherInterface $path_matcher, CurrentPathStack $currentPathStack) {
    parent::__construct($state, $config_factory);

    $this->request = $request_stack->getCurrentRequest();
    $this->pathMatcher = $path_matcher;
    $this->currentPathStack = $currentPathStack;
  }

  /**
   * {@inheritdoc}
   */
  public function exempt(AccountInterface $account) {

    // Standard core behavior - check user's permission.
    if ($account->hasPermission('access site in maintenance mode')) {
      return TRUE;
    }

    // Check if the IP address should be exempted.
    $client_ip = $this->request->getClientIp();
    if (in_array($client_ip, maintenance_exempt_get_ips())) {
      return TRUE;
    }
    if (maintenance_exempt_by_cidr_notation($client_ip)) {
      return TRUE;
    }

    // Check if the URL should be exempted.
    $exempt_urls = $this->config->get('maintenance_exempt.settings')->get('exempt_urls');
    if (!empty($exempt_urls)) {
      // Check the actual URL.
      $current_url = $this->request->getPathInfo();
      if ($this->pathMatcher->matchPath($current_url, $exempt_urls)) {
        return TRUE;
      }
    }

    // Check the system path of aliased paths.
    $current_path = $this->currentPathStack->getPath();
    if ($this->pathMatcher->matchPath($current_path, $exempt_urls)) {
      return TRUE;
    }

    // Fetch the query string exemption key if there is one.
    $key = $this->config->get('maintenance_exempt.settings')->get('query_key');

    // Exemption status may be stored in the session.
    if (isset($_SESSION['maintenance_exempt']) && $_SESSION['maintenance_exempt'] == $key) {
      return TRUE;
    }

    if ($key && isset($_GET[$key])) {
      $_SESSION['maintenance_exempt'] = $key;
      return TRUE;
    }

    // No valid exemption, so user remains blocked.
    return FALSE;
  }

}
